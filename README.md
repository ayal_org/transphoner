# Transphoner

Transphoner allows you to set rules on phone numbers. Rules have conditions and actions, and can be used to change
the outgoing number on-the-fly e.g. to add a calling card access number.

Conditions allow testing which SIM you have, and which network you are connected to. Conditions can be grouped with AND and OR.
Actions allow to postfix, prefix, or replace parts of the number.

Transphoner includes a UI for creating and modifying rules and a call interceptor which transforms dialed numbers
according to the rules. Rules can be exported and imported using the clipboard

# Beta-Testing
Join the beta-testing community at [Google+](https://plus.google.com/communities/115891576997527613563)

# Bug Reports, Feature Requests ....
Please use the [issues](https://bitbucket.org/ayal_org/transphoner/issues) page to report bugs and request new features

# Building

Transphoner can be built using Android Studio or just using gradle.

If you want to produce a signed release, add a `local.gradle` file with your keys:
```
android {
    signingConfigs {
        release {
            storeFile file("/path/to/android/keystore")
            storePassword "t0p$ecret"
            keyAlias "MyAlias"
            keyPassword "pa$$w0rd"
        }
    }
}
```

# License

Transphoner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Transphoner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.