/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

import java.util.ArrayList;

public class CallInfo {
    private final Context context;
    private final String OrigNumber;
    private final PhoneNumber NormalizedNumber;
    private String CurrentNumber;
    private ArrayList<String> FiredRules = new ArrayList<String>();
    public final static int UNKNOWN = 0;

    /**
     * Initialize the CallInfo class. If preference set, will try to normalize number according to default country code.
     * If the normalization fails or resulting number is invalid, will leave number as as. If not, it will normalize
     * number even before any actions are applied.
     *
     * @param context The application context
     * @param origNumber Original number
     */
    @SuppressWarnings("EmptyCatchBlock")
    CallInfo (Context context, String origNumber) {
        PhoneNumber _NormalizedNumber;
        this.context = context;

        // Try to normalize number if user requested
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences (context);
        _NormalizedNumber = null;
        if (prefs.getBoolean ("Normalize", false)) {

            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance ();
            try {
                _NormalizedNumber = phoneUtil.parse (origNumber, prefs.getString ("CountryCode", "").toUpperCase ());

                // if this is not a valid number, then we won't normalize
                if (!phoneUtil.isValidNumber (_NormalizedNumber)) _NormalizedNumber = null;
            } catch (NumberParseException e) {
                // left empty intentionally
            }

            if (_NormalizedNumber!= null) {
                origNumber = phoneUtil.format (_NormalizedNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
            }
        }
        NormalizedNumber = _NormalizedNumber;
        OrigNumber = origNumber;
        // Note: If user requests, number is normalized even before any rule is applied
        CurrentNumber = origNumber;
    }

    public void setCurrentNumber (String currentNumber) {
        CurrentNumber = currentNumber;
    }

    public Context getContext () { return context;}

    public String getOrigNumber () {
        return OrigNumber;
    }

    public String getCurrentNumber () {
        return CurrentNumber;
    }

    public void addFiredRule (String name) {FiredRules.add(name);}
    public ArrayList<String> getFiredRules () {return FiredRules;}

    /**
     *
     * @return country code or UNKNOWN
     */
    public int getCountryCode () {
        if (NormalizedNumber == null) return UNKNOWN;
        else return NormalizedNumber.getCountryCode ();
    }
}
