/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.Conditions;

import android.content.Context;

import org.ayal.Transphoner.CallInfo;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public abstract class Condition {
    boolean isNegate = false;
    public void setNegate (boolean isNegate) {
        this.isNegate = isNegate;
    }
    public boolean isNegate () {
        return isNegate;
    }

    Condition (boolean isNegate) {
        this.isNegate = isNegate;
    }

    Condition (String isNegate) {
        this.isNegate = isNegate.toLowerCase ().equals ("true");
    }

    public String getName () {
        return this.getClass ().getSimpleName ();
    }

    /*
    evaluates the condition as if isNegate = false
     */
    protected abstract boolean _evaluate (CallInfo callInfo);

    public boolean evaluate (CallInfo callInfo) {
        // really! a XOR does it!
        return isNegate ^ _evaluate (callInfo);
    }

    public abstract String getArgument ();

    public abstract String toString();

    public abstract String describe();

    boolean Compare (String s1, String s2) {
        return s1.toLowerCase ().equals (s2.toLowerCase ());
    }

    /*
    A condition can additionally implement a method with the following signature:
    public static String getCurrentValue (Context context)
    which will return the current value of the  condition.
    This can help the user define the condition
    */
    @SuppressWarnings("EmptyCatchBlock")
    public static String getCurrentValue (String CondName, Context context) {
        try {
            Class<?> cls = Class.forName ("org.ayal.Transphoner.Conditions." + CondName);
            Class<?> param[] = new Class[1];
            param[0] = Context.class;
            Object args[] = new Object[1];
            args[0] = context;
            return (String) cls.getDeclaredMethod ("getCurrentValue", param).invoke (null,args);
        } catch (ClassNotFoundException e) {
        } catch (NoSuchMethodException e) {
        } catch (InvocationTargetException e) {
        } catch (IllegalAccessException e) {
        }
        return "";
    }

    public static Condition fromString (String conditionName, String conditionArgument) {
        return fromString (conditionName, new String[] {conditionArgument});
    }

    @SuppressWarnings("EmptyCatchBlock")
    public static Condition fromString (String conditionName, String [] conditionArgument) {
        try {
            Class<?> cls = Class.forName ("org.ayal.Transphoner.Conditions." + conditionName);
            Class<?> param[] = new Class[conditionArgument.length];
            Object paramVal[] = new Object[conditionArgument.length];
            for (int i=0; i< conditionArgument.length; i++) {
                param[i] = String.class;
                paramVal[i] = conditionArgument[i];
            }
            Constructor<?> ct = cls.getConstructor (param);
            return (Condition) ct.newInstance (paramVal);
        } catch (ClassNotFoundException e) {
        } catch (NoSuchMethodException e) {
        } catch (InvocationTargetException e) {
        } catch (InstantiationException e) {
        } catch (IllegalAccessException e) {
        }
        return null;
    }

    // Add new condition names here
    static public final String[] AvailableConditions = {
            "AND",
            "OR",
            "NetworkCountryIso",
            "NetworkOperator",
            "NetworkOperatorName",
            "SimCountryIso",
            "SimOperator",
            "SimOperatorName",
            "StartsWith",
            "NumberOfDigits"
    };
}
