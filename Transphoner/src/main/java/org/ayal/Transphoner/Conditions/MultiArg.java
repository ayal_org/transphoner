/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.Conditions;

public abstract class MultiArg extends Condition {

    ConditionList SubConditions;

    MultiArg (ConditionList subConditions) {
        super (false);
        this.SubConditions = subConditions;
    }

    @Override
    public String toString() {
        String retval = this.getClass ().getSimpleName () + "(";
        for (Condition cond: SubConditions)  {
            if (cond == null) continue;
            retval += cond + "$";
        }
        return retval + ")";
    }

    @Override
    public String describe () {
        boolean first = true;
        String retval = "(";

        for (Condition cond: SubConditions)  {
            if (cond == null) continue;
            if (!first) retval += " " + this.getClass ().getSimpleName () + " ";
            retval += cond.describe ();
            first = false;
        }
        return retval + ")";
    }


    MultiArg () {
        super (false);
        this.SubConditions = new ConditionList ();
    }

    public void addSubCondition (Condition cond) {
        SubConditions.add (cond);
    }

    public void setSubConditions (ConditionList subConditions) {
        SubConditions = subConditions;
    }

    public ConditionList getSubConditions () {
        return SubConditions;
    }

}
