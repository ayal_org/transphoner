/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.Conditions;

import android.util.Log;

import org.ayal.Transphoner.CallInfo;

public class StartsWith extends OneArg {
    private static final String THIS_FILE = "StartsWith";

    public StartsWith (String argument) {
        super (argument);
    }

    public StartsWith (String argument, boolean isCaseSensitive){
        super (argument,isCaseSensitive);
    }

    public StartsWith (String argument, String isCaseSensitive){
        super (argument,isCaseSensitive);
    }

    @Override
    public boolean _evaluate (CallInfo callInfo) {
        boolean res = callInfo.getOrigNumber ().toLowerCase ().startsWith(argument.toLowerCase());
        Log.d(THIS_FILE, "argument<"+argument.toLowerCase ()+"> Orig<" + callInfo.getOrigNumber ().toLowerCase ()+">");
        Log.d(THIS_FILE, "IsNegate "+ isNegate);
        Log.d(THIS_FILE, "Result: "+ res);
        return res;
    }

    @Override
    public String describe () {
        return "Number " + (isNegate ? "does not start " : "starts ") + "with " + argument;
    }
}
