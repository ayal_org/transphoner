/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.Conditions;

import org.ayal.Transphoner.CallInfo;

import java.util.ArrayList;

public class ConditionList extends ArrayList<Condition> {

    public boolean all (CallInfo callinfo) {
        for (Condition next : this) {
            if (next != null && !next.evaluate (callinfo)) {
                return false;
            }
        }
        return true;
    }

    public boolean any (CallInfo callInfo) {
        for (Condition next : this) {
            if (next != null && next.evaluate (callInfo)) {
                return true;
            }
        }
        return false;
    }


    public void addByString (String conditionName, String conditionArgument) {
        Condition newCondition = Condition.fromString (conditionName, conditionArgument);
        if (newCondition==null) return;
        add (newCondition);
    }

    public void setByString (String conditionName, String conditionArgument, int position) {
        Condition newCondition = Condition.fromString(conditionName,conditionArgument);
        if (newCondition==null) return;

        set (position, newCondition);
    }
}
