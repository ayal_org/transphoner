/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.Conditions;

public abstract class OneArg extends Condition{
    final String argument;

    @Override
    public String getArgument () {
        return argument;
    }

    OneArg (String argument) {
        super(false);
        this.argument = argument;
    }

    OneArg (String argument, boolean isCaseSensitive){
        super (isCaseSensitive);
        this.argument = argument;
    }

    OneArg (String argument, String isCaseSensitive){
        super (isCaseSensitive);
        // can't have $ in an argument -- we use it as a field separator
        this.argument = argument.replace ('$','_');
    }

    @Override
    public String toString() {
        return getName () + "(" + argument + "$" + isNegate + ")";
    }

    @Override
    public String describe () {
        return getName () + " is " + (isNegate ? "not " : "") +argument;
    }
}
