/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.Conditions;

import android.content.Context;
import android.telephony.TelephonyManager;
import org.ayal.Transphoner.CallInfo;

public class NetworkOperator extends OneArg{
    public NetworkOperator (String argument) {
        super (argument);
    }

    public static String getCurrentValue (Context context) {
        TelephonyManager tm = (TelephonyManager)context.getSystemService (Context.TELEPHONY_SERVICE);
        return tm.getNetworkOperator ();
    }

    public NetworkOperator (String argument, boolean isCaseSensitive){
        super (argument,isCaseSensitive);
    }

    public NetworkOperator (String argument, String isCaseSensitive){
        super (argument,isCaseSensitive);
    }

    @Override
    public boolean _evaluate (CallInfo callInfo) {
        TelephonyManager tm = (TelephonyManager)callInfo.getContext ().getSystemService (Context.TELEPHONY_SERVICE);
        return Compare (tm.getNetworkOperator (), argument);
    }
}
