/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.Conditions;

import org.ayal.Transphoner.CallInfo;

public class OR extends MultiArg {

    public OR () {super();}
    public OR (ConditionList conditions) {super (conditions);}

    @Override
    public String getName () {
        return "OR";
    }

    @Override
    public boolean _evaluate (CallInfo callInfo) {
        return SubConditions.any (callInfo);
    }

    @Override
    public String getArgument () {
        return "";
    }
}
