/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.Conditions;

import org.ayal.Transphoner.CallInfo;

/**
 * Created by shaia on 11/24/14.
 */
public class NumberOfDigits extends OneArg {

    public NumberOfDigits(String argument) {
        super (argument);
    }

    public NumberOfDigits (String argument, boolean isCaseSensitive){
        super (argument,isCaseSensitive);
    }

    public NumberOfDigits (String argument, String isCaseSensitive){
        super (argument,isCaseSensitive);
    }

    @Override
    protected boolean _evaluate(CallInfo callInfo) {
        try {
            return callInfo.getOrigNumber().length() == Integer.parseInt(argument);
        }
        catch (NumberFormatException e) {
            return false;
        }
    }
}
