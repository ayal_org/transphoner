/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.Actions;

import android.util.Log;

public class ReplaceBeginning extends Action {
    private static final String THIS_FILE = "ReplaceBeginning";
    private final String From;
    private final String To;

    public ReplaceBeginning (String from, String to) {
        if (from == null) from = "";
        if (to == null) to = "";

        From = from.replace ('$','_');
        To = to.replace ('$','_');
    }

    @Override
    public String Act (String OrigNumber) {
        Log.d(THIS_FILE, "From<"+From+"> To<"+To+"> Orig<"+OrigNumber+">");


        if (OrigNumber.startsWith (From)) return To + OrigNumber.substring(From.length());
        Log.d(THIS_FILE,"No replacement");
        return OrigNumber;
    }

    @Override
    public String[] getArgumentNames () {
        String[] ret = new String[2];
        ret[0] = "From";
        ret[1] = "To";
        return ret;
    }

    @Override
    public String[] getArguments () {
        String[] retval = new String [2];
        retval[0] = From;
        retval[1] = To;
        return retval;
    }

    @Override
    public String toString () {
        return getName () + "(" + From + "$" + To + ")";
    }

    @Override
    public String describe () {
        return "Replace " + From + " at the beginning of the number with " + To;
    }
}
