/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.Actions;

public class Null extends Action {

    @Override
    public String Act (String OrigNumber) {
        return OrigNumber;
    }

    @Override
    public String describe () {
        return "No Action";
    }

    @Override
    public String[] getArgumentNames () { return new String[0];}

    @Override
    public String[] getArguments () {
        return new String[0];
    }

    @Override
    public String toString () {
        return "";
    }
}
