/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.Actions;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public abstract class Action {
    public abstract String Act (String OrigNumber) ;
    public abstract String describe ();
    public abstract String[] getArgumentNames ();
    public abstract String[] getArguments();

    public String getName () {
        return this.getClass ().getSimpleName ();
    }
    // Add new action names here
    static public final String[] AvailableActions = {
            "Null",
            "Postfix",
            "Prefix",
            "ReplaceBeginning",
    };

    @SuppressWarnings("EmptyCatchBlock")
    public static Action fromString (String actionName, String [] actionArgument) {
        try {
            Class<?> cls = Class.forName ("org.ayal.Transphoner.Actions." + actionName);
            Class<?>[] param = new Class[actionArgument.length];
            Object[] paramVal = new Object[actionArgument.length];
            for (int i = 0; i < actionArgument.length; i++) {
                param[i] = String.class;
                paramVal[i] = actionArgument[i];
            }
            Constructor<?> ct = cls.getConstructor (param);
            return (Action) ct.newInstance (paramVal);
        } catch (ClassNotFoundException e) {
        } catch (NoSuchMethodException e) {
        } catch (InvocationTargetException e) {
        } catch (InstantiationException e) {
        } catch (IllegalAccessException e) {
        }
        return new Null();
    }
}
