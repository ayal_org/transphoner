/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.Actions;

import java.util.ArrayList;

/**
 * Created by shaia on 9/10/14.
 */
public class ActionList extends ArrayList<Action> {
    public void addByString (String actionName, String[] actionArguments) {
        Action newAction = Action.fromString(actionName, actionArguments);
        add (newAction);
    }

    public void setByString (String actionName, String[] actionArguments, int position) {
        Action newAction = Action.fromString(actionName, actionArguments);
        set (position, newAction);
    }

    public String Act (String OrigNumber) {
        String NewNumber = OrigNumber;
        for (Action action : this) {
            if (action == null) continue;
            NewNumber = action.Act(NewNumber);
        }
        return NewNumber;
    }

    public String describe () {
        String description = "";
        for (Action action : this) {
            if (action == null) continue;
            if (description.length() > 0) description += " AND ";
            description += action.describe();
        }
        return description;
    }

    @Override
    public String toString() {
        String tostring = "(";
        for (Action action : this) {
            if (action == null) continue;
            tostring += action.toString() + "$";
        }
        return tostring + ")";
    }
}
