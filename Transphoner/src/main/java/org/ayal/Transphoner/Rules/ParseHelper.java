/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.Rules;

import java.util.ArrayList;

class ParseHelper {
    private static String[] ParseWithParenthesis (String input, char delimiter) {
        ArrayList<String> retval = new ArrayList<String> ();
        int parenthesis_counter = 0;
        int last_delimiter = 0;
        int i;
        for (i = 0; i < input.length (); i++) {
            if (input.charAt (i) == '(') parenthesis_counter++;
            else if (input.charAt (i) == ')') parenthesis_counter--;


            if (parenthesis_counter < 0) break;
            if (input.charAt (i) == delimiter || parenthesis_counter < 0) {
                if (parenthesis_counter <= 0 && i > 0) {
                    retval.add (input.substring (last_delimiter, i));
                    last_delimiter = i + 1;
                }
            }
        }
        retval.add (input.substring (last_delimiter, i));
        return retval.toArray (new String[retval.size ()]);
    }

    static String[] ParseWithParenthesis (String input) {
        return ParseWithParenthesis (input, '$');
    }
}
