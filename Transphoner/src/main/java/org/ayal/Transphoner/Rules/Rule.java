/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.Rules;

import org.ayal.Transphoner.Actions.ActionList;
import org.ayal.Transphoner.CallInfo;
import org.ayal.Transphoner.Conditions.AND;

public class Rule {

    private AND Conditions = new AND();

    private ActionList Actions = new ActionList();
    private String Name = "";

    public Rule () {}

    public String getName () { return Name; }

    public AND getConditions () {
        return Conditions;
    }

    public void setConditions (AND conditions) {
        Conditions = conditions;
    }

    public ActionList getActions () {
        return Actions;
    }

    public void setActions (ActionList actions) {
        Actions = actions;
    }


    public void setName (String name) {
        Name = name;
    }

    public Rule (String name, AND conditions, ActionList actions) {
        Conditions = conditions;
        Actions = actions;
        Name = name;
    }

    public CallInfo go (CallInfo callInfo) {
        if (Conditions.evaluate (callInfo)) {
            callInfo.addFiredRule(getName());
            callInfo.setCurrentNumber(Actions.Act (callInfo.getCurrentNumber()));
        }
        return callInfo;
    }

    public String toString () {
        return Name + ":" + Conditions + "$" + Actions;
    }
}
