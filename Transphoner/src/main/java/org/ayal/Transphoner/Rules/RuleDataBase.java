/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.Rules;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import org.ayal.Transphoner.Actions.Action;
import org.ayal.Transphoner.Actions.ActionList;
import org.ayal.Transphoner.Actions.Null;
import org.ayal.Transphoner.CallInfo;
import org.ayal.Transphoner.Conditions.AND;
import org.ayal.Transphoner.Conditions.Condition;
import org.ayal.Transphoner.Conditions.OR;

public class RuleDataBase {

    private static final String THIS_FILE = "RuleDataBase";

    public CallInfo Process (CallInfo callinfo) {
        RuleList Rules = UnDump (callinfo);
        for (Rule rule : Rules) {
            Log.d(THIS_FILE,"CurrentNumber <"+callinfo.getCurrentNumber ()+">");
            callinfo= rule.go(callinfo);
        }
        return callinfo;
    }

    void Dump (Context context, RuleList Rules) {
        PreferenceManager.getDefaultSharedPreferences (context)
                .edit ()
                .putString ("Rules", DumpToString (Rules))
                .commit ();
    }

    public int size (Context context) {
        RuleList Rules = UnDump (context);
        return Rules.size ();
    }

    public Rule get (Context context, int i) {
        RuleList Rules = UnDump (context);
        return Rules.get (i);
    }

    public void add (Context context, Rule object) {
        RuleList Rules = UnDump (context);
        Rules.add (object);
        Dump(context, Rules);
    }

    public void set (Context context, Rule rule, int position) {
        RuleList Rules = UnDump (context);
        Rules.set (position, rule);
        Dump (context, Rules);
    }

    public void remove (Context context, int position) {
        RuleList Rules = UnDump (context);
        Rules.remove (position);
        Dump (context, Rules);
    }

    RuleList getRules (Context context) {
        return UnDump (context);
    }

    public String DumpToString(Context context) {
        return DumpToString (getRules (context));
    }

    String DumpToString (RuleList Rules) {
        String retval = "";
        for (Rule rule : Rules) {
            retval += rule + "\n";
        }
        return retval;
    }

    RuleList UnDump (CallInfo callInfo) {
        return UnDump (callInfo.getContext ());
    }

    RuleList UnDump (Context context) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences (context);
        return UnDump (prefs.getString ("Rules",""));
    }

    static public Rule UnDumpRule (String input) {
        String rulename[] = input.split (":", 2);
        if (rulename.length == 2) {
            String[] RuleArgs = ParseHelper.ParseWithParenthesis (rulename[1]);
            if (RuleArgs.length == 2) {
                AND Conditions = (AND) UnDumpCondition (RuleArgs[0]);
                ActionList Act = UnDumpActions (RuleArgs[1]);
                return new Rule (rulename[0],Conditions,Act);
            }
        }
        return null;
    }

    private static RuleList UnDump (String input) {
        RuleList NewRules = new RuleList ();

        for (String line : input.split ("\n")) {
            Rule rule = UnDumpRule (line);
            if (rule != null) NewRules.add(rule);
        }
        return NewRules;
    }

    public static Condition UnDumpCondition (String input) {
        String[] Split = input.split ("\\(", 2);
        if (Split.length == 2) {
            String CondName = Split[0];
            String[] args = ParseHelper.ParseWithParenthesis (Split[1]);
            if (CondName.toLowerCase ().equals ("and")) {
                AND And = new AND ();
                for (String subcond : args) {
                    Condition tmpCond = UnDumpCondition (subcond);
                    if (tmpCond != null) And.addSubCondition (tmpCond);
                }
                return And;
            }
            if (CondName.toLowerCase ().equals ("or")) {
                OR Or = new OR ();
                for (String subcond : args) {
                    Condition tmpCond = UnDumpCondition (subcond);
                    if (tmpCond != null) Or.addSubCondition (tmpCond);
                }
                return Or;
            }
            return Condition.fromString (CondName, args);
        }
        return null;
    }

    public static ActionList UnDumpActions (String input) {
        ActionList Actions = new ActionList();

        // remove leading ( if exists
        if (input.startsWith("("))   {
            input = input.substring(1);
        }

        String[] actions = ParseHelper.ParseWithParenthesis(input);
        for (String strAction : actions) {
            if (strAction.length() ==0) continue;
            Actions.add(UnDumpAction(strAction));
        }
        return Actions;
    }

    public static Action UnDumpAction (String input) {
        String[] Split = input.split ("\\(", 2);
        if (Split.length == 2) {
            String ActName = Split[0];
            String[] args = ParseHelper.ParseWithParenthesis (Split[1]);
            return Action.fromString (ActName, args);
        }
        return new Null();
    }
}
