/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.ayal.Transphoner.Rules.RuleDataBase;

public class InterceptCall extends BroadcastReceiver {

    public void onReceive (Context context, Intent intent) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences (context);
        String action = intent.getAction ();
        String number = getResultData ();

        // Escape if no number
        if (number == null) {
            // No reformatted number, use the original
            number = intent.getStringExtra (Intent.EXTRA_PHONE_NUMBER);
            if (number == null) return;
        }

        if (prefs.getBoolean ("Enabled", false)
                &&
                (action != null && action.equals (Intent.ACTION_NEW_OUTGOING_CALL))
                &&
                !PhoneNumberUtils.isEmergencyNumber (number)
                ) {

            final String THIS_FILE = "InterceptCall";
            Log.d (THIS_FILE, "Raw Number:" + number);

            String newnumber = PhoneNumberUtils.stripSeparators (PhoneNumberUtils.convertKeypadLettersToDigits (number));
            Log.d (THIS_FILE, "Striped & Converted :" + newnumber);

            CallInfo newCI = new RuleDataBase ().Process (new CallInfo (context, newnumber));

            int toast = Integer.parseInt(prefs.getString("Toast", "3"));
            if ( (toast == 2 && newCI.getFiredRules().size()>0)
                ||
                (toast == 3)) {
                NiceToast(newCI).show();
            }

            number = newCI.getCurrentNumber();
        }

        // Pass the call to pstn handle
        setResultData (number);
    }

    private Toast NiceToast (CallInfo ci) {
        LayoutInflater inflater = (LayoutInflater) ci.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View layout = inflater.inflate(R.layout.custom_toast,null);

        ((TextView) layout.findViewById (R.id.orig_number)).setText (ci.getOrigNumber());
        ((TextView) layout.findViewById (R.id.final_number)).setText (ci.getCurrentNumber());
        Toast toast = new Toast(ci.getContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);

        return toast;
    }
}
