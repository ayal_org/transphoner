/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.UI;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.ayal.Transphoner.Actions.Action;
import org.ayal.Transphoner.Actions.Null;
import org.ayal.Transphoner.R;
import org.ayal.Transphoner.Rules.RuleDataBase;

import java.util.ArrayList;
import java.util.Arrays;

public class ModifyActionDialog extends DialogFragment {
    private final static String ARGUMENT_ACTION = "ACTION";

    private Action action;
    private final ArrayList<View> argumentViews = new ArrayList<View> ();
    private Spinner spinnerActions;

    public static ModifyActionDialog newInstance (Action action) {
        ModifyActionDialog d = new ModifyActionDialog ();
        Bundle args = new Bundle ();
        args.putString (ARGUMENT_ACTION, action.toString ());
        d.setArguments (args);
        return d;
    }

    public ModifyActionDialog() {
    }

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        action = RuleDataBase.UnDumpAction (getArguments ().getString (ARGUMENT_ACTION));
        if (getTargetFragment () != null) {
            try {
                // note that this happens after onAttach !
                mListener = (NoticeDialogListener) getTargetFragment();
            } catch (ClassCastException e) {
                e.printStackTrace ();
            }
        }
    }

    public interface NoticeDialogListener {
        public void onDialogPositiveClick(Action action);
    }
    private NoticeDialogListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) getTargetFragment();
        } catch (ClassCastException e) {
            mListener = null;
        }
    }

    private void onActionUpdated (View view, String actionName) {

        // try to make an action with up to 5 arguments
        int argLen;
        Action actionLocal;
        for (argLen=0, actionLocal = new Null (); argLen<5 && actionLocal.getName ().equals ("Null"); argLen++)
            actionLocal = Action.fromString (actionName, new String[argLen]);

        if (actionLocal != null) {
            if (actionLocal.getClass ().equals (action.getClass ())) {
                actionLocal = action;
            }

            // Add arguments as needed
            LinearLayout ll = (LinearLayout) view.findViewById (R.id.root_layout);
            ll.removeAllViews ();
            argumentViews.clear ();
            for (int i=0; i<actionLocal.getArgumentNames ().length; i++) {
                View argView = getActivity ().getLayoutInflater ().inflate (R.layout.action_argument, ll, false);
                ((TextView) argView.findViewById (R.id.argument_name)).setText (actionLocal.getArgumentNames ()[i]);
                ((TextView) argView.findViewById (R.id.argument_value)).setText (actionLocal.getArguments ()[i]);
                ll.addView (argView);
                argumentViews.add (argView);
            }
        }
    }

    private Action getSelectedAction () {
        String[] arguments = new String[argumentViews.size ()];
        for (int i=0; i<argumentViews.size (); i++) {
            arguments[i] = ((EditText)argumentViews.get(i).findViewById (R.id.argument_value)).getText ().toString ();
        }

        return Action.fromString ((String) spinnerActions.getSelectedItem (),arguments);
    }

    public Dialog onCreateDialog (Bundle savedInstanceState) {
        // inflate layout
        LayoutInflater inflater = getActivity ().getLayoutInflater ();
        final View dialogView = inflater.inflate (R.layout.dialog_modify_action, null);

        ArrayAdapter<String> actionAdapter = new ArrayAdapter<String>(inflater.getContext(),
                android.R.layout.simple_spinner_item,
                Action.AvailableActions);
        actionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerActions = (Spinner) dialogView.findViewById (R.id.action_spinner);
        spinnerActions.setAdapter (actionAdapter);
        spinnerActions.setOnItemSelectedListener (new AdapterView.OnItemSelectedListener () {
            @Override
            public void onItemSelected (AdapterView<?> adapterView, View view, int position, long l) {
                onActionUpdated (dialogView, Action.AvailableActions[position]);
            }

            @Override
            public void onNothingSelected (AdapterView<?> adapterView) {

            }
        });
        spinnerActions.setSelection (Arrays.asList (Action.AvailableActions).indexOf (action.getName ()));
        onActionUpdated (dialogView, action.getName ());

        AlertDialog.Builder builder = new AlertDialog.Builder (getActivity ());
        builder.setView (dialogView)
                .setTitle (getString(R.string.action))
                .setPositiveButton (R.string.ok, new DialogInterface.OnClickListener () {
                    @Override
                    public void onClick (DialogInterface dialog, int id) {
                        mListener.onDialogPositiveClick (getSelectedAction ());
                    }
                })
                .setNegativeButton (R.string.cancel, new DialogInterface.OnClickListener () {
                    public void onClick (DialogInterface dialog, int id) {
                        getDialog ().cancel ();
                    }
                });

        // Create the AlertDialog object and return it
        return builder.create ();
    }

}
