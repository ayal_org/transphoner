/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.UI;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.ayal.Transphoner.R;

public class AdvancedFragment extends Fragment implements TextEditDialog.TextEditDialogListener{
    TextView networkInfo = null;
    TextView rulesText = null;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate (R.layout.fragment_advanced,container,false);

        networkInfo = (TextView) view.findViewById(R.id.network_info);
        rulesText = (TextView) view.findViewById(R.id.rules_text);

        view.findViewById(R.id.copy_network_info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CopyTextToClipboard(networkInfo.getText());
            }
        });

        view.findViewById(R.id.copy_rules).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CopyTextToClipboard(rulesText.getText());
            }
        });

        view.findViewById(R.id.edit_rules).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextEditDialog dialog = TextEditDialog.newInstance (rulesText.getText().toString());
                dialog.setTargetFragment(AdvancedFragment.this, -1);
                dialog.show (getActivity().getSupportFragmentManager(), "");
            }
        });

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        TelephonyManager tm = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        String NetworkInfo =
                "Network Country ISO: " + tm.getNetworkCountryIso () + "\n" +
                        "Network Operator: " + tm.getNetworkOperator () + "\n" +
                        "Network Operator Name: " + tm.getNetworkOperatorName () + "\n" +
                        "SIM Country ISO: " + tm.getSimCountryIso () + "\n" +
                        "SIM Operator: " + tm.getSimOperator () + "\n" +
                        "SIM Operator Name: " + tm.getSimOperatorName ();
        networkInfo.setText(NetworkInfo);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        rulesText.setText(prefs.getString("Rules", ""));
    }

    protected void CopyTextToClipboard (CharSequence text) {
        if (Build.VERSION.SDK_INT < 11) {
            //noinspection deprecation
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText (text);
        } else {
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setPrimaryClip (ClipData.newPlainText("Transphoner", text));
        }
        Toast.makeText(getActivity(), "Copied to clipboard", Toast.LENGTH_SHORT).show ();
    }

    @Override
    public void onDialogPositiveClick(String txt) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        prefs.edit().putString("Rules", txt).commit();
        rulesText.setText(txt);
    }
}
