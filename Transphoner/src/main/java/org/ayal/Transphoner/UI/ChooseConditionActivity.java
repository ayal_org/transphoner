/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import org.ayal.Transphoner.Conditions.Condition;
import org.ayal.Transphoner.Conditions.MultiArg;
import org.ayal.Transphoner.R;

public class ChooseConditionActivity
        extends ActionBarActivity
        implements ChooseConditionFragment.OnFragmentInteractionListener,
                    ModifyConditionDialog.NoticeDialogListener {

    public final static String PARAM_CONDITION = "CONDITION";
    private static final int REQUEST_NEW_CONDITION = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView (R.layout.activity_choose_condition);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction ()
                    .add(ChooseConditionFragment.newInstance () ,"")
                    .commit ();
        }
    }

    @Override
    public void onFragmentInteraction (Condition condition) {
        try {
            MultiArg multiArg = (MultiArg) condition;
            Intent intent = new Intent (this, MultiArgActivity.class);
            intent.putExtra (MultiArgActivity.ARG_CONDITION, multiArg.toString ());
            startActivityForResult (intent, REQUEST_NEW_CONDITION);

        } catch (ClassCastException e) {
            ModifyConditionDialog dialog = ModifyConditionDialog.newInstance (condition);
            dialog.show (getSupportFragmentManager (),"");
        }

    }

    @Override
    public void onDialogPositiveClick (Condition condition) {
        Intent it = new Intent ();
        it.putExtra (PARAM_CONDITION, condition.toString ());
        setResult (ConditionListFragment.RESULT_OK, it);
        finish ();
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult (requestCode, resultCode, data);
        if (requestCode==REQUEST_NEW_CONDITION && resultCode==ConditionListFragment.RESULT_OK) {
            Intent it = new Intent ();
            it.putExtra (PARAM_CONDITION, data.getExtras ().getString (ChooseConditionActivity.PARAM_CONDITION));
            setResult (ConditionListFragment.RESULT_OK, it);
            finish ();
        }
    }
}
