    /*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.UI;

    import android.app.Activity;
    import android.app.AlertDialog;
    import android.app.Dialog;
    import android.content.DialogInterface;
    import android.os.Bundle;
    import android.support.v4.app.DialogFragment;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.widget.Button;
    import android.widget.CheckBox;
    import android.widget.TextView;
    import org.ayal.Transphoner.Conditions.Condition;
    import org.ayal.Transphoner.R;
    import org.ayal.Transphoner.Rules.RuleDataBase;

    public class ModifyConditionDialog extends DialogFragment {

        private TextView argumentTextView;
        private CheckBox negate;

        private final static String ARGUMENT_CONDITION = "CONDITION";

        private Condition condition;

        public static ModifyConditionDialog newInstance (Condition condition) {
            ModifyConditionDialog d = new ModifyConditionDialog ();
            Bundle args = new Bundle();
            args.putString (ARGUMENT_CONDITION, condition.toString ());
            d.setArguments (args);
            return d;
        }

        public ModifyConditionDialog () {
        }

        @Override
        public void onCreate (Bundle savedInstanceState) {
            super.onCreate (savedInstanceState);
            condition = RuleDataBase.UnDumpCondition (getArguments ().getString (ARGUMENT_CONDITION));
            if (getTargetFragment () != null) {
                try {
                    // note that this happens after onAttach !
                    mListener = (NoticeDialogListener) getTargetFragment();
                } catch (ClassCastException e) {
                    e.printStackTrace ();
                }
            }
        }

        @Override
        public Dialog onCreateDialog (Bundle savedInstanceState) {
            // inflate layout
            LayoutInflater inflater = getActivity ().getLayoutInflater ();
            View dialogView = inflater.inflate (R.layout.dialog_modify_condition, null);

            argumentTextView = (TextView) dialogView.findViewById (R.id.condition_argument);
            argumentTextView.setText (condition.getArgument () );

            Button getCurr = (Button) dialogView.findViewById (R.id.get_current_value);
            getCurr.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick (View view) {
                    argumentTextView.setText (Condition.getCurrentValue (condition.getName (), getActivity ()));
                    argumentTextView.requestFocus ();
                }
            });

            negate = (CheckBox) dialogView.findViewById (R.id.negate);
            negate.setChecked (condition.isNegate ());

            AlertDialog.Builder builder = new AlertDialog.Builder (getActivity ());
            builder.setView (dialogView)
                    .setTitle (condition.getName ())
                    .setPositiveButton (condition.getArgument () != null ? R.string.modify : R.string.add, new DialogInterface.OnClickListener () {
                        @Override
                        public void onClick (DialogInterface dialog, int id) {
                            Condition locCondition = Condition.fromString (
                                    condition.getName (),
                                    argumentTextView.getText ().toString ());
                            locCondition.setNegate (negate.isChecked ());
                            mListener.onDialogPositiveClick (locCondition);
                        }
                    })
                    .setNegativeButton (R.string.cancel, new DialogInterface.OnClickListener () {
                        public void onClick (DialogInterface dialog, int id) {
                            getDialog ().cancel ();
                        }
                    });

            // Create the AlertDialog object and return it
            return builder.create ();
        }


        // pass data back to activity
        public interface NoticeDialogListener {
            public void onDialogPositiveClick(Condition condition);
        }
        private NoticeDialogListener mListener;

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            // Verify that the host activity implements the callback interface
            try {
                // Instantiate the NoticeDialogListener so we can send events to the host
                mListener = (NoticeDialogListener) activity;
            } catch (ClassCastException e) {
                mListener = null;
            }
        }
    }
