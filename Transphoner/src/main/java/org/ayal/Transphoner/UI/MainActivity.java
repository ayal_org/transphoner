/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.UI;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.CheckBox;

import org.ayal.Transphoner.R;

import java.io.IOException;
import java.util.Scanner;

public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        // Initialize the "Enable" checkbox
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        CheckBox enable = (CheckBox) findViewById(R.id.enabled);
        enable.setChecked (prefs.getBoolean ("Enabled", false));
        enable.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick (View view) {
                CheckBox cb = (CheckBox) view;
                SharedPreferences.Editor edt =
                        PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit();
                edt.putBoolean ("Enabled", cb.isChecked ());
                edt.commit ();
            }
        });

        findViewById(R.id.donate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowHTML("Donate", "feedback.html", getResources().getString(R.string.git_rev));
            }
        });

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments

        Fragment curFragment = null;
        switch (position) {
            case 0:
                curFragment = new DialingRulesFragment();
                break;
            case 1:
                curFragment = new AdvancedFragment();
                break;
            case 2:
                curFragment = new SettingsFragment();
                break;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, curFragment)
                .commit();
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.transphoner, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_other_licenses:
                ShowHTML("Open Source Licenses", "licenses.html");
                break;

            case R.id.action_about:
                ShowHTML(null, "about.html", getResources().getString(R.string.git_rev));
                break;

            case R.id.action_license:
                ShowHTML("License", "gpl-3.0-standalone.html");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void ShowHTML(String title, String filename) {
        ShowHTML (title,filename,null);
    }

    public void ShowHTML(String title, String filename, String version) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        if (title != null) alert.setTitle(title);
        final WebView wv = new WebView (this);
        AssetManager am = this.getAssets();
        try {
            String rawHTML = new Scanner(am.open(filename)).useDelimiter("\\A").next();
            if (version != null) {
                rawHTML = rawHTML.replace ("$version", version);
            }
            wv.loadData(rawHTML, "text/html", "UTF-8");
            alert.setView(wv);
            alert.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
