/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.UI;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import org.ayal.Transphoner.R;

/**
 * Created by shaia on 11/29/14.
 */
public class TextEditDialog extends DialogFragment {

    private final static String ARGUMENT_TEXT = "ACTION";
    String initialText = "";


    public static TextEditDialog newInstance (String txt) {
        TextEditDialog d = new TextEditDialog ();
        Bundle args = new Bundle ();
        args.putString (ARGUMENT_TEXT, txt);
        d.setArguments (args);
        return d;
    }

    public TextEditDialog() {};

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        initialText = getArguments ().getString (ARGUMENT_TEXT);
        if (getTargetFragment () != null) {
            try {
                // note that this happens after onAttach !
                mListener = (TextEditDialogListener) getTargetFragment();
            } catch (ClassCastException e) {
                e.printStackTrace ();
            }
        }
    }

    public Dialog onCreateDialog (Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity ().getLayoutInflater ();
        final View dialogView = inflater.inflate (R.layout.dialog_textedit, null);
        final EditText text = (EditText) dialogView.findViewById (R.id.edit_text);
        text.setText(initialText);
        AlertDialog.Builder builder = new AlertDialog.Builder (getActivity ());
        builder.setView(dialogView)
            .setTitle(getString(R.string.edit))
            .setPositiveButton (R.string.ok, new DialogInterface.OnClickListener () {
                @Override
                public void onClick (DialogInterface dialog, int id) {
                    mListener.onDialogPositiveClick (text.getText().toString());
                }
            })
            .setNegativeButton (R.string.cancel, new DialogInterface.OnClickListener () {
                public void onClick (DialogInterface dialog, int id) {
                    getDialog ().cancel ();
                }
            });

        return builder.create();
    }

    public interface TextEditDialogListener {
        public void onDialogPositiveClick(String txt);
    }
    private TextEditDialogListener mListener;

}
