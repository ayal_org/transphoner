/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.UI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.contextualundo.ContextualUndoAdapter;

import org.ayal.Transphoner.R;
import org.ayal.Transphoner.Rules.Rule;
import org.ayal.Transphoner.Rules.RuleDataBase;

public class DialingRulesFragment extends Fragment implements ContextualUndoAdapter.DeleteItemCallback {

    private RulesAdapter rules_adapter = null;
    private ContextualUndoAdapter undoAdapter;


    static final public String EXTRA_RULE_ID = "EXTRA_RULE_ID";

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        rules_adapter = new RulesAdapter(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate (R.layout.fragment_dialing_rules,container,false);

        // initialize the list of rules
        ListView rules = (ListView) view.findViewById(R.id.rules_list);
        undoAdapter = new ContextualUndoAdapter (
                rules_adapter,
                R.layout.undo_row,
                R.id.undo_row_undobutton,
                5000,
                this);
        undoAdapter.setAbsListView (rules);
        rules.setAdapter (undoAdapter);

        rules.setOnItemClickListener (new AdapterView.OnItemClickListener () {
            @Override
            public void onItemClick (AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent (getActivity(), RuleActivity.class);
                intent.putExtra (EXTRA_RULE_ID, rules_adapter.getItem (position).toString ());
                // Note position+1! 0 is reserved for adding a new result
                startActivityForResult (intent, position+1);
            }
        });

        View btn = view.findViewById(R.id.add_rule);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addRule();
            }
        });

        return view;
    }


    private void addRule () {
        Intent intent = new Intent (getActivity(), RuleActivity.class);
        startActivityForResult (intent, 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode== RuleActivity.RESULT_OK) {
            Rule rule = RuleDataBase.UnDumpRule (data.getStringExtra (EXTRA_RULE_ID));
            if (requestCode == 0) rules_adapter.add (rule);
            else rules_adapter.set(requestCode-1, rule);
        }
    }

    @Override
    public void deleteItem (int position) {
        rules_adapter.remove (position);
    }

    @Override
    public void onPause() {
        super.onPause ();
        undoAdapter.removePendingItem ();
    }

    public class RulesAdapter extends BaseAdapter {
        private final Context context;
        private final RuleDataBase rules = new RuleDataBase ();

        public RulesAdapter (Context context) {
            this.context = context;
        }

        @Override
        public int getCount () {
            return rules.size (context);
        }

        @Override
        public Object getItem (int i) {
            return rules.get (context, i);
        }

        public void remove (int i) {
            rules.remove (context, i);
            notifyDataSetChanged ();
        }

        @Override
        public long getItemId (int i) {
            return getItem (i).hashCode ();
        }

        public void add (Rule newRule) {
            rules.add (context, newRule);
            notifyDataSetChanged ();
        }

        public void set (int position, Rule newRule) {
            rules.set (context, newRule, position);
            notifyDataSetChanged ();
        }

        @Override
        public View getView (int position, View convertView, ViewGroup parent) {
            View rowView = convertView;
            if (rowView== null)
                rowView = LayoutInflater.from (context).inflate(R.layout.item_rule, parent, false);

            TextView name = (TextView) rowView.findViewById (R.id.rule_name);
            TextView conditions = (TextView) rowView.findViewById (R.id.rule_condition);
            TextView action = (TextView) rowView.findViewById (R.id.rule_action);

            Rule rule = rules.get (context, position);
            name.setText (rule.getName ());
            conditions.setText (rule.getConditions ().describe ());
            action.setText (rule.getActions().describe ());

            return rowView;
        }
    }

}
