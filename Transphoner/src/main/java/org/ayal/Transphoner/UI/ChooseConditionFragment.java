/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.UI;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import org.ayal.Transphoner.Conditions.Condition;
import org.ayal.Transphoner.R;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChooseConditionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChooseConditionFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class ChooseConditionFragment extends ListFragment {

    private OnFragmentInteractionListener mListener;

   public static ChooseConditionFragment newInstance() {
        return new ChooseConditionFragment ();
    }

    public ChooseConditionFragment () {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setListAdapter (new ArrayAdapter<String> (
                inflater.getContext (),
                android.R.layout.simple_list_item_1,
                Condition.AvailableConditions)
        );

        return inflater.inflate (R.layout.fragment_choose_condition,container,true);
    }

    @Override
    public void onListItemClick (ListView l, View v, int position, long id) {
        super.onListItemClick (l, v, position, id);
        String[] zeroString = new String[0];
        String[] oneString = new String[1];
        String conditionName = (String) l.getItemAtPosition (position);

        Condition condition = Condition.fromString (conditionName,zeroString);
        if (condition==null) condition = Condition.fromString (conditionName,oneString);

        if (condition!=null) mListener.onFragmentInteraction (condition);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Condition condition);
    }

}
