/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.ayal.Transphoner.Conditions.MultiArg;
import org.ayal.Transphoner.R;
import org.ayal.Transphoner.Rules.RuleDataBase;

public class MultiArgActivity extends ActionBarActivity
        implements ListFragment.ListFragmentInteractionListener {

    private MultiArg multiArg;
    private ConditionListFragment clFragment;

    public static final String ARG_CONDITION = "MULTIARG_CONDITION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            multiArg = (MultiArg) RuleDataBase.UnDumpCondition (extras.getString (ARG_CONDITION));
        }
        else {
            return;
        }

        setContentView (R.layout.activity_multiarg);

        if (savedInstanceState == null) {
            clFragment = ConditionListFragment.newInstance (multiArg);
            FragmentTransaction ft = getSupportFragmentManager ().beginTransaction();
            ft.replace (R.id.titles, clFragment).commit();
        }

        ((TextView) findViewById (R.id.textViewConditions)).setText (multiArg.getName ());
        final LayoutInflater inflater = (LayoutInflater) getSupportActionBar ().getThemedContext ()
                .getSystemService (LAYOUT_INFLATER_SERVICE);
        final View customActionBarView = inflater.inflate(
                R.layout.actionbar_custom_view_done_cancel, null);
        customActionBarView.findViewById (R.id.actionbar_done).setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick (View v) {
                clFragment.CleanUp ();
                Intent I = new Intent ();
                I.putExtra (ChooseConditionActivity.PARAM_CONDITION, multiArg.toString ());
                setResult (ConditionListFragment.RESULT_OK, I);
                finish ();
            }
        });
        customActionBarView.findViewById (R.id.actionbar_cancel).setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick (View v) {
                // "Cancel"
                finish ();
            }
        });

        // Show the custom action bar view and hide the normal Home icon and title.
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(
                ActionBar.DISPLAY_SHOW_CUSTOM,
                ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME
                        | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setCustomView(customActionBarView,
                new android.support.v7.app.ActionBar.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));

    }

    @Override
    public void onFragmentInteraction (String arg) {
        MultiArg condition = (MultiArg) RuleDataBase.UnDumpCondition(arg);
        multiArg.setSubConditions (condition.getSubConditions());
    }
}
