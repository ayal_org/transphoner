/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.UI;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import org.ayal.Transphoner.Actions.Action;
import org.ayal.Transphoner.Actions.ActionList;
import org.ayal.Transphoner.Actions.Null;
import org.ayal.Transphoner.R;
import org.ayal.Transphoner.Rules.RuleDataBase;

/**
 * Created by shaia on 9/10/14.
 */
public class ActionListFragment
        extends ListFragment
        implements ModifyActionDialog.NoticeDialogListener {

    final static int REQUEST_ADD_ITEM = 1;
    int clicked_position = 0;

    public static ActionListFragment newInstance(ActionList actions) {
        ActionListFragment fragment = new ActionListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CONDITION, actions.toString ());
        fragment.setArguments(args);
        return fragment;
    }

    public ActionListFragment () {
    }

    @Override
    public void initAdapter (String arg) {
        ActionList actions = RuleDataBase.UnDumpActions(arg);
        listAdapter = new ActionListAdapter (getActivity (), actions);
    }

    @Override
    void addItem() {
        ModifyActionDialog dialog = ModifyActionDialog.newInstance (new Null());
        dialog.setTargetFragment(this, REQUEST_ADD_ITEM);
        clicked_position = -1;
        dialog.show (getActivity().getSupportFragmentManager(), "");
    }

    @Override
    void modifyItem(AdapterView<?> adapterView, View view, int position, long id) {
        ModifyActionDialog dialog = ModifyActionDialog.newInstance((Action)listAdapter.getItem(position));
        dialog.setTargetFragment(this, REQUEST_ADD_ITEM);
        clicked_position = position;
        dialog.show (getActivity().getSupportFragmentManager(), "");
    }

    @Override
    public void onDialogPositiveClick(Action action) {
        if (clicked_position<0) {
            ((ActionListAdapter) listAdapter).add(action);
        }
        else {
            ((ActionListAdapter) listAdapter).set(clicked_position, action);
        }
        mListener.onFragmentInteraction (listAdapter.toString());
    }

    private class ActionListAdapter extends ListAdapter {

        public ActionListAdapter(Context context, ActionList actionList) {
            super(context, actionList);
        }

        public void set ( int position, Action newAction) {
            ((ActionList) list).set (position, newAction);
            notifyDataSetChanged ();
        }

        public void add(Action newAction) {
            ((ActionList) list).add(newAction);
            notifyDataSetChanged();
        }

        @Override
        public String toString() {
            return ((ActionList) list).toString();
        }

        @Override
        public View getView (int position, View convertView, ViewGroup parent) {
            ActionList actions = ((ActionList) list);
            TextView tv = (TextView) convertView;
            if (tv == null)
                tv = (TextView) LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);

            if (position < actions.size ())
                tv.setText (actions.get (position).describe ());
            return tv;
        }
    }

}
