/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.ayal.Transphoner.Actions.ActionList;
import org.ayal.Transphoner.Conditions.AND;
import org.ayal.Transphoner.Conditions.MultiArg;
import org.ayal.Transphoner.R;
import org.ayal.Transphoner.Rules.Rule;
import org.ayal.Transphoner.Rules.RuleDataBase;

public class RuleActivity extends ActionBarActivity
        implements
        ListFragment.ListFragmentInteractionListener,
        ruleNameDialog.NoticeDialogListener {

    private static final String ARG_RULE = "SavedRule";
    private Rule rule=new Rule ();
    private ConditionListFragment clFragment;
    private ActionListFragment acFragment;

    public static final int RESULT_OK = 1;
    public static final int RESULT_CANCEL = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();

        if (savedInstanceState != null) { // resumed
            rule = RuleDataBase.UnDumpRule(savedInstanceState.getString(ARG_RULE));
        } else if (extras != null) { // Edit existing rule
            rule = RuleDataBase.UnDumpRule(extras.getString(DialingRulesFragment.EXTRA_RULE_ID));
        } else { // Add new rule
            rule = new Rule();
        }

        setContentView(R.layout.activity_rule);
    }
    @Override
    protected void onResume() {
        super.onResume();

        ((TextView) findViewById (R.id.rule_name)).setText (rule.getName());
        findViewById (R.id.rule_name).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ruleNameDialog dialog = ruleNameDialog.newInstance(rule.getName());
                dialog.show(getSupportFragmentManager(),"");
            }
        });

        acFragment = ActionListFragment.newInstance(rule.getActions());
        clFragment = ConditionListFragment.newInstance (rule.getConditions ());

        getSupportFragmentManager ().beginTransaction()
                .replace (R.id.rule_action, acFragment)
                .replace (R.id.titles, clFragment)
                .commit();

        final LayoutInflater inflater = (LayoutInflater) getSupportActionBar ().getThemedContext ()
                .getSystemService (LAYOUT_INFLATER_SERVICE);
        final View customActionBarView = inflater.inflate(
                R.layout.actionbar_custom_view_done_cancel, null);
        customActionBarView.findViewById (R.id.actionbar_done).setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick (View v) {
                clFragment.CleanUp();
                acFragment.CleanUp();
                Intent I = new Intent ();
                I.putExtra (DialingRulesFragment.EXTRA_RULE_ID, rule.toString ());
                setResult (RESULT_OK, I);
                finish ();
            }
        });
        customActionBarView.findViewById (R.id.actionbar_cancel).setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick (View v) {
                // "Cancel"
                finish ();
            }
        });

        // Show the custom action bar view and hide the normal Home icon and title.
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(
                ActionBar.DISPLAY_SHOW_CUSTOM,
                ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME
                        | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setCustomView(customActionBarView,
                new ActionBar.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
   }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(ARG_RULE, rule.toString());
    }

    @Override
    public void onFragmentInteraction (String arg) {
        MultiArg condition = (MultiArg) RuleDataBase.UnDumpCondition(arg);
        if (condition != null) {
            rule.setConditions(new AND(condition.getSubConditions()));
        }
        else {
            ActionList actions = RuleDataBase.UnDumpActions(arg);
            if (actions != null) {
                rule.setActions(actions);
            }
        }
    }

    @Override
    public void onRuleNamePositiveClick(CharSequence rule_name) {
        rule.setName(rule_name.toString());
        ((TextView) findViewById (R.id.rule_name)).setText(rule.getName());
    }
}
