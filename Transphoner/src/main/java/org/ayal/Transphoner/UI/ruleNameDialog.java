/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.UI;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import org.ayal.Transphoner.R;

/**
* Created by shaia on 7/13/14.
*/
public class ruleNameDialog extends DialogFragment {
    private static final String ARGUMENT_NAME = "NAME";

    public interface NoticeDialogListener {
        public void onRuleNamePositiveClick(CharSequence rule_name);
    }

    private TextView ruleNameView;
    private String ruleName;


    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    public static ruleNameDialog newInstance (String ruleName) {
        ruleNameDialog d = new ruleNameDialog();
        Bundle args = new Bundle();
        args.putString (ARGUMENT_NAME, ruleName);
        d.setArguments (args);
        return d;
    }

    public ruleNameDialog() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ruleName=getArguments().getString(ARGUMENT_NAME);
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = inflater.inflate(R.layout.fragmet_rule_name, null);
        ruleNameView = (TextView) view.findViewById(R.id.rule_name);
        ruleNameView.setText(ruleName);
        builder.setView(view).
                setTitle(R.string.rule_name).
                setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getDialog().cancel();
                    }
                }).
                setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mListener.onRuleNamePositiveClick(ruleNameView.getText());
                    }
                });

        return builder.create();

    }
}
