/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.UI;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.contextualundo.ContextualUndoAdapter;

import org.ayal.Transphoner.R;

import java.util.ArrayList;

/**
 * Created by shaia on 9/14/14.
 */
public abstract class ListFragment extends Fragment
        implements View.OnClickListener,
        ContextualUndoAdapter.DeleteItemCallback  {

    protected static final String ARG_CONDITION = "param1";

    private static final int AUTO_DELETE_TIMEOUT = 5000;

    protected ListFragmentInteractionListener mListener;

    protected ListAdapter listAdapter;
    protected ContextualUndoAdapter undoAdapter;

    abstract void initAdapter(String argString);
    abstract void addItem();
    abstract void modifyItem (AdapterView<?> adapterView, View view, int position, long id);

    public ListFragment () {
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String argString;
        if (savedInstanceState != null) {
            argString = savedInstanceState.getString (ARG_CONDITION);
        }
        else {
            argString = getArguments ().getString (ARG_CONDITION);
        }

        initAdapter (argString);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        View btn = view.findViewById (R.id.add_item);
        btn.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick (View view) {
                addItem();
            }
        });

        ListView lst = (ListView) view.findViewById (R.id.list);

        lst.setOnItemClickListener (new AdapterView.OnItemClickListener () {
            @Override
            public void onItemClick (AdapterView<?> adapterView, View view, int position, long id) {
                modifyItem (adapterView, view, position, id);
            }
        });

        undoAdapter = new ContextualUndoAdapter (
                listAdapter,
                R.layout.undo_row,
                R.id.undo_row_undobutton,
                5000,
                this);
        undoAdapter.setAbsListView (lst);
        lst.setAdapter (undoAdapter);

        setHasOptionsMenu (true);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach (activity);
        try {
            mListener = (ListFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach ();
        mListener = null;
    }

    public void CleanUp () {
        undoAdapter.removePendingItem ();
        mListener.onFragmentInteraction (listAdapter.toString());
    }

    @Override
    public void onPause () {
        super.onPause ();
        CleanUp ();
    }

    @Override
    public void deleteItem (int i) {
        listAdapter.remove (i);
    }

    @Override
    public void onSaveInstanceState (Bundle outState) {
        super.onSaveInstanceState(outState);
        String str = listAdapter.toString ();
        outState.putString(ARG_CONDITION, str);
    }

    public interface ListFragmentInteractionListener {
        public void onFragmentInteraction(String str);
    }

    public abstract class ListAdapter extends BaseAdapter {
        protected ArrayList<?> list;
        protected final Context context;

        public ListAdapter(Context context, ArrayList<?> List) {
            this.context = context;
            this.list = List;
        }

        public void remove (int i) {
            list.remove(i);
            notifyDataSetChanged ();
        }

        @Override
        public int getCount () {
            return list.size ();
        }

        @Override
        public Object getItem (int i) {
            return list.get (i);
        }

        @Override
        public long getItemId (int i) {
            return getItem (i).hashCode ();
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }

    @Override
    public void onClick(View view) {
    }
}
