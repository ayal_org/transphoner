/*
 * Copyright (c) 2014 By Shai Ayal
 *
 * This file is part of Transphoner.
 *
 * Transphoner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Transphoner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Transphoner.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.ayal.Transphoner.UI;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import org.ayal.Transphoner.Conditions.Condition;
import org.ayal.Transphoner.Conditions.ConditionList;
import org.ayal.Transphoner.Conditions.MultiArg;
import org.ayal.Transphoner.R;
import org.ayal.Transphoner.Rules.RuleDataBase;


public class ConditionListFragment
        extends ListFragment
        implements ModifyConditionDialog.NoticeDialogListener {

    private static final int REQUEST_ADD_CONDITION = 1;
    private static final int REQUEST_MODIFY_CONDITION = 2;

    public static final int RESULT_OK = 3;
    public static final int RESULT_ADD_CONDITION = 4;

    private int clicked_position;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param condition Condition to modify
     * @return A new instance of fragment ConditionListFragment.
     */
    public static ConditionListFragment newInstance(MultiArg condition) {
        ConditionListFragment fragment = new ConditionListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CONDITION, condition.toString());
        fragment.setArguments(args);
        return fragment;
    }

    public ConditionListFragment() {
        // Required empty public constructor
    }


    @Override
    void initAdapter(String argString) {
        MultiArg condition = (MultiArg) RuleDataBase.UnDumpCondition(argString);
        listAdapter = new ConditionListAdapter(getActivity(), condition);

    }

    @Override
    public void onClick(View view) {
    }


    void addItem() {
        startActivityForResult(new Intent(getActivity(), ChooseConditionActivity.class), REQUEST_ADD_CONDITION);
    }

    @Override
    void modifyItem(AdapterView<?> adapterView, View view, int position, long id) {
        Condition tmpCondition = (Condition) listAdapter.getItem(position);
        clicked_position = position;
        try {
            MultiArg multiArg = (MultiArg) tmpCondition;
            Intent intent = new Intent(getActivity(), MultiArgActivity.class);
            intent.putExtra(MultiArgActivity.ARG_CONDITION, multiArg.toString());
            startActivityForResult(intent, REQUEST_MODIFY_CONDITION);

        } catch (ClassCastException e) {
            ModifyConditionDialog dialog = ModifyConditionDialog.newInstance(tmpCondition);
            dialog.setTargetFragment(ConditionListFragment.this, 0);
            dialog.show(getActivity().getSupportFragmentManager(), "");
        }
    }

    @Override
    public void onDialogPositiveClick(Condition condition) {
        // from ModifyConditionDialog
        ((ConditionListAdapter) listAdapter).set(clicked_position, condition);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // from ChooseConditionActivity
        if (requestCode == REQUEST_ADD_CONDITION) {
            if (resultCode == RESULT_OK) ((ConditionListAdapter) listAdapter).add(
                    RuleDataBase.UnDumpCondition(data.getExtras().getString(ChooseConditionActivity.PARAM_CONDITION)));
        }
        // from MultiArgActivity
        if (requestCode == REQUEST_MODIFY_CONDITION) {
            if (resultCode == RESULT_OK) ((ConditionListAdapter) listAdapter).set(
                    clicked_position,
                    RuleDataBase.UnDumpCondition(data.getExtras().getString(ChooseConditionActivity.PARAM_CONDITION)));
        }
        mListener.onFragmentInteraction (listAdapter.toString());
    }

    public class ConditionListAdapter extends ListAdapter {

        public ConditionList getConditions() {
            return (ConditionList) list;
        }

        private final MultiArg multiArg;

        public ConditionListAdapter(Context context, MultiArg ma) {
            super(context, ma.getSubConditions());
            this.multiArg = ma;
        }


        public void set(int position, Condition newCondition) {
            ((ConditionList) list).set(position, newCondition);
            notifyDataSetChanged();
        }

        public void add(Condition newCondition) {
            ((ConditionList) list).add(newCondition);
            notifyDataSetChanged();
        }

        @Override
        public String toString() {
            multiArg.setSubConditions(getConditions());
            return multiArg.toString();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView tv = (TextView) convertView;
            if (tv == null)
                tv = (TextView) LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);

            if (position < list.size())
                tv.setText(((ConditionList) list).get(position).describe());
            return tv;
        }
    }
}
